package cl.ubb.determinemosnumeroprimo;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;

public class determinemosnumeroperfectotest {

	@Test
	public void IngresarSeisRetornaNumeroPerfectoVerdadero() {
		/*arrange*/
		determinemosnumeroperfecto perfecto = new determinemosnumeroperfecto();
		boolean resultado;
		
		/*act*/ 
		resultado = perfecto.determinemosnumeroperfecto(6);
		
		/*assert*/
		assertThat(resultado,is(true));
		
	}

}
